module.exports = (app, STATIC_ROOT) => {
    //login enrty

    console.log('ABCDE')

    app.post('/login', (req, res, next) => {
        passport.authenticate('local', (err, user, info) => {
            //DB error
            if(err){
                return res.json({
                    status: 'error',
                    message: info.message
                });
            }

            //unauthenticated
            if(!user){
                return res.json({
                    status: 'error',
                    message: info.message
                });
            }

            //authenticated
            req.logIn(user, err => {
                if(err){
                    return next(err);
                }
                return res.json({
                    status: 'success'
                });
            });
        })(req, res, next);
    })
    // app.post('/login', (req, res, next) => {
    //     passport.authenticate('local', (err, user, info) => {
    //         //DB error
    //         if(err){
    //             return res.json({
    //                 status: 'error',
    //                 message: info.message
    //             });
    //         }

    //         //unauthenticated
    //         if(!user){
    //             return res.json({
    //                 status: 'error',
    //                 message: info.message
    //             });
    //         }

    //         //authenticated
    //         req.logIn(user, err => {
    //             if(err){
    //                 return next(err);
    //             }
    //             return res.json({
    //                 status: 'success'
    //             });
    //         });
    //     })(req, res, next);
    // });

    // //logout entry
    // app.get('/logout', (req, res) => {
    //     context = "/";
    //     if(req.headers['x-context-path']){
    //         context = req.headers['x-context-path']
    //     }
    //     req.logout();
    //     res.redirect(context);
    // });

    //route root
    app.get('/', (req, res) => {
        // if(req.user){ //if logged in
            //send main entry
            res.sendFile('main/index.html', {root: STATIC_ROOT});
        // } else { //not logged in
        //     //send login page
        //     res.sendFile('login/index.html', {root: STATIC_ROOT});
        // }
    });

    //redirect html page
    app.get('*.html', (req, res, next) => {
        if(!req.user){
            res.sendStatus(403);
        } else {
            next();
        }
    });

    // API
    require('../api/product/product-route')(app);
    // require('../api/user/user-route')(app);
    // require('../api/app/app-route')(app);
    // require('../api/issue/issue-route')(app);
    // require('../api/crash/crash-route')(app);
}
