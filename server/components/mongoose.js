const mongoose   = require('mongoose');

const DB_HOST = 'localhost:27017';
const DB_NAME = 'ManSys';

module.exports = {
    connect: () => new Promise(resolve => {
        mongoose.connect(`${DB_HOST}/${DB_NAME}`);
        // db = connect("localhost:27017/myDatabase")

        const db = mongoose.connection;

        db.on('open', () => {
            console.log(`Connected to ${db.name}`);
            resolve();
        });

        db.on('error', (err) => {
            console.error('Connection error:', err);
            console.log('Process terminated');
            process.exit(1);
        });

        process.on('SIGINT', () => {
            db.close(() => {
                console.log('Mongoose disconnected due to app termination');
                process.exit(0);
            });
        });
    })
}
