const userService = require('./user-service')

module.exports = (app) => {
   // Get Me
  app.get('/api/user/me', (req, res) => {
    // if(!req.user){
      res.sendStatus(403);
      return;
    // }
    
    res.json({
      _id: req.user._id,
      username: req.user.username
    })
  });
}
