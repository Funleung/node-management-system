// DB
Product = require('./product-schema')

const LIMIT = 10

const getProducts = () => {
	return Product.find()
}

let find = (query = {}, project = {}, opts = {}) => {
	// Promise
	return Product.find()
}

let findOneById = (id = {}) => {
	// Promise
	return Product.findById({_id: id})
}

module.exports = {
	find,
	findOneById,
	getProducts
}