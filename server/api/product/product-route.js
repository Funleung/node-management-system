const productService = require('./product-service')

module.exports = app => {
	app.get('api/products', (req, res) => {
		// if(!req.user) {
		// 	res.sendStatus(403)
		// 	return;
		// }

		// console.log("Enter product service")

		productService.getProducts(req.query)
			.then(products => {
				res.json(products)
			})
			.catch((err) => {
        res.sendStatus(500);
      });
	})
}