let mongoose = require('mongoose')

mongoose.Promise = global.Promise;

const ProductSchema = mongoose.Schema({
	"name" : String,
	"price" : String,
	"desc" : String
});

module.exports = mongoose.model('product', ProductSchema)