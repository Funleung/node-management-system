//load node module
const express    = require('express');
const session    = require('express-session');
const bodyParser = require('body-parser');

//load local module
const mongoose   = require('./components/mongoose');
// const passport   = require('./components/passport');


//local constraints
const STATIC_ROOT = `${__dirname}/static`;
const APP_PORT = 3000;

// App
const app = express();
app.set('port', APP_PORT);

// Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(session({
  secret: 'mansys',
  cookie: { maxAge: 900000 },
  resave: true,
  saveUninitialized: false
}));
// app.use(passport.initialize());
// app.use(passport.session());

// Route
const main = require('./components/route')(app, STATIC_ROOT);

// Static
app.use(express.static(STATIC_ROOT));

// Connect to Mongoose
mongoose.connect().then(() => {

  // Listen
  app.listen(APP_PORT, () => {
    console.log(`Listening on port ${app.get('port')}`);
  });
})